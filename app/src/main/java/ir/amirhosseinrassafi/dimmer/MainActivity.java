package ir.amirhosseinrassafi.dimmer;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import ir.amirhosseinrassafi.dimmer.Connections.MqttClient;
import ir.amirhosseinrassafi.dimmer.Connections.TcpClient;

public class MainActivity extends AppCompatActivity {

    ListView lv;
    Context context;
    Integer cnt = 0 ;

    MqttClient mqttClient;
    static ArrayList<String> prgmName;

    public static int [] prgmImages={};
    public static String [] prgmNameList={};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        context=this;

        TcpClient.setAddress("192.168.43.154");
        TcpClient.setPort(8888);

        mqttClient = new MqttClient("172.25.1.1", 1883);
        MqttClient.setContext(this.getApplicationContext());
        //tc.sendMessageAndGetResponse("hello world\n");

        prgmName = new ArrayList<>();
        for(int i = 0 ; i < prgmNameList.length; i++)
        {
            prgmName.add(prgmNameList[i]);
        }
        lv=(ListView) findViewById(R.id.listView);
        lv.setAdapter(new CustomAdapter(this, prgmName,prgmImages));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                addtolist("Node " + ++cnt);
                NodeData temp = new NodeData(cnt);
                NodeLib.addNode(temp);

                lv.invalidateViews();
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });

        FloatingActionButton connect = (FloatingActionButton) findViewById(R.id.Connect);
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Settings.getMqtt())
                {
                    mqttClient.connect();
                }
                else
                {
                    TcpClient.connect(context);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(MainActivity.this, Settings.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void addtolist(String name)
    {
        prgmName.add(name);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TcpClient.disconnect();
        MqttClient.disConnect();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}