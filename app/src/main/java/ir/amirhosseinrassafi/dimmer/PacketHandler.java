package ir.amirhosseinrassafi.dimmer;

import android.os.Handler;
import android.os.Looper;

import ir.amirhosseinrassafi.dimmer.Connections.MqttClient;
import ir.amirhosseinrassafi.dimmer.Connections.TcpClient;

public class PacketHandler {

    static UiHandler handler;
    private PacketHandler()
    {}

    static void sendNodeData(NodeData nd)
    {
        byte temp[] = {nd.getNodeID().byteValue(), nd.getDimmers()[0], nd.getDimmers()[1],'\n'};
        if(Settings.getMqtt())
        {
            MqttClient.publishMessage(temp);
        }
        else
        {
            TcpClient.sendMessage(temp);
        }
    }

    public static void receivedNodeData(byte[] dataIn)
    {
        /* packet format :  ID - LEDs - Temp[2] - humidity[2] - Ldr */
       /* Integer id = (int) dataIn[2];
        System.out.println("recive : " + dataIn[0] +" " +  dataIn[1] + " " + dataIn[2] + " " + dataIn[3] +" " +  (dataIn[4]+1) );
        Byte[] leds = new Byte[2];
        if(dataIn[4] == 1) {
            leds[0] = 1;
        }else{
            leds[0] = 0;
        }
        if(dataIn[5] == 1) {
            leds[1] = 1;
        }else{
            leds[1] = 0;
        }

        NodeData nd = NodeLib.getNodeData(id);
        nd.setLeds(leds);

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                handler.handle();
            }
        });*/
    }

    public static void setHandler(UiHandler handler) {
        PacketHandler.handler = handler;
    }
}