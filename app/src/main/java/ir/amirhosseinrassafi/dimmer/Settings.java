package ir.amirhosseinrassafi.dimmer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import ir.amirhosseinrassafi.dimmer.Connections.MqttClient;
import ir.amirhosseinrassafi.dimmer.Connections.TcpClient;

public class Settings extends AppCompatActivity {

    EditText TCP_ip, TCP_port;
    EditText Mqtt_ip, Mqtt_port;
    Button setTCP, setMqtt;
    Switch Mqtt;
    private static Boolean MQTT = new Boolean(false);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        TCP_ip = (EditText) findViewById(R.id.TCP_ip);
        TCP_port = (EditText) findViewById(R.id.TCP_port);
        setTCP = (Button) findViewById(R.id.setTCP);

        Mqtt_ip = (EditText) findViewById(R.id.Mqtt_ip);
        Mqtt_port = (EditText) findViewById(R.id.Mqtt_port);
        setMqtt = (Button) findViewById(R.id.setMqtt);
        Mqtt = (Switch) findViewById(R.id.MQTT);

        Mqtt.setChecked(MQTT);
        TCP_ip.setText(TcpClient.getAddress());
        TCP_port.setText(String.valueOf(TcpClient.getPort()));

        Mqtt_ip.setText(MqttClient.getAddress());
        Mqtt_port.setText(String.valueOf(MqttClient.getPort()));

        setTCP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TcpClient.setAddress(TCP_ip.getText().toString());
                TcpClient.setPort(Integer.valueOf(TCP_port.getText().toString()));
                Toast.makeText(getApplicationContext(),
                        "TCP IP & Port seted", Toast.LENGTH_SHORT).show();
            }
        });

        setMqtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MqttClient.setAddress(Mqtt_ip.getText().toString());
                MqttClient.setPort(Integer.valueOf(Mqtt_port.getText().toString()));
                Toast.makeText(getApplicationContext(),
                        "MQTT IP & Port seted", Toast.LENGTH_SHORT).show();
            }
        });

        Mqtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                System.out.println(((Switch) v).isChecked());
                MQTT = ((Switch) v).isChecked();
                if(MQTT == true)
                {
                    Toast.makeText(getApplicationContext(),
                            "Internet Selected", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),
                            "Local Selected", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public static Boolean getMqtt(){
        return MQTT;
    }

}
