package ir.amirhosseinrassafi.dimmer;

public class NodeData {

    private Integer nodeID;
    private Byte[] dimmers;
    private Node n;

    public NodeData(Integer nodeID)
    {
        this.nodeID = nodeID;
        this.dimmers = new Byte[2];
        this.dimmers[0] = new Byte("0");
        this.dimmers[1] = new Byte("0");
    }

    public Integer getNodeID() {
        return nodeID;
    }

    public void setNodeID(Integer nodeID) {
        this.nodeID = nodeID;
    }

    public Byte[] getDimmers() {
        return dimmers;
    }

    public void setDimmers(Byte[] dimmers) {
        this.dimmers[0] = dimmers[0];
        this.dimmers[1] = dimmers[1];
    }

    public void setNode(Node n)
    {
        this.n = n;
    }

    public void update()
    {
        n.upDateNode();
    }

}
