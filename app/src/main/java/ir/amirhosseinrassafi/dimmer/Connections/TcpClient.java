package ir.amirhosseinrassafi.dimmer.Connections;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;

import ir.amirhosseinrassafi.dimmer.PacketHandler;
import ir.amirhosseinrassafi.dimmer.UiHandler;

public class TcpClient {
    static private String dstAddress;
    static private int dstPort;
    static public Socket client;
    static InputStream inputStream;
    static UiHandler handler;
    private TcpClient(Context context, String address, int port)
    {
        this.dstAddress = address;
        this.dstPort = port;
    }



    static public void setPort(int port)
    {
        dstPort = port;
    }

    static public int getPort()
    {
        return dstPort;
    }

    static public void setAddress(String address)
    {
        dstAddress = address;
    }

    static public String getAddress()
    {
        return dstAddress;
    }

    static public void connect(final Context context)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    client = new Socket(dstAddress, dstPort);
                    inputStream = client.getInputStream();
                    if(client != null)
                    {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, "Connected to Server",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    try{
                        while(true) {
                            client.getOutputStream().write(255);
                            Thread.sleep(1500);
                        }
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, "DisConnected from Server",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();


        new Thread(new Runnable() {
            @Override
            public void run() {
                byte buff[] = new byte[300];
                byte tempbuff[] =  new byte[7];
                while (true) {
                    if(client != null) {
                        if(client.isConnected() & inputStream != null) {
                            try {
                                int size = inputStream.available();
                                if (size > 6) {

                                    inputStream.read(buff);
                                    System.out.println("inputdata : " + size);
                                    for (int i = 0; i < size; i++) {
                                        System.out.print( buff[i] + " ");
                                    }
                                    if( buff[0] == -1 & buff[1] == -1) {
                                        for (int i = 0; i < 7; i++) {
                                            tempbuff[i] = buff[i];
                                        }
                                        PacketHandler.receivedNodeData(buff);
                                    }
                                    System.out.println();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }).start();
    }

    static public void disconnect()
    {
        if(client != null) {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static public String sendMessage(final byte[] data) {
        String response = null;
        if(client != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        DataOutputStream dataOutputStream = new DataOutputStream(client.getOutputStream());
                        dataOutputStream.write(data);
                    } catch (IOException ex) {
                        Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }).start();
            return response;
        }
        return null;
    }

}