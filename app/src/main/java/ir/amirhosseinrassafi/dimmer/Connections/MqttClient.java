package ir.amirhosseinrassafi.dimmer.Connections;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import ir.amirhosseinrassafi.dimmer.PacketHandler;

public class MqttClient {

    static private String dstAddress;
    static private int dstPort;
    static MqttAndroidClient mqttAndroidClient;
    static private final MemoryPersistence persistence = new MemoryPersistence();
    static private String serverUri ;
    static String clientId = "ExampleAndroidClient";

    static final String subscriptionTopic = "hubs";
    static final String publishTopic = "hubp";

    final String publishMessage = "Hello World!";

    static private Context context;

    public MqttClient(String Address, int Port){
        clientId = clientId + System.currentTimeMillis();
        dstAddress = Address;
        dstPort = Port;
        serverUri = "tcp://" + dstAddress + ":" + dstPort;
    }

    static public void setPort(int port)
    {
        dstPort = port;
    }

    static public int getPort()
    {
        return dstPort;
    }

    static public void setAddress(String address)
    {
        dstAddress = address;
    }

    static public String getAddress()
    {
        return dstAddress;
    }

    static public void setContext(Context cont)
    {
        context = cont;
    }

    static public void connect(){
        serverUri = "tcp://" + dstAddress + ":" + dstPort;
        System.out.println(serverUri);

        mqttAndroidClient = new MqttAndroidClient(context, serverUri, clientId, persistence);
        mqttAndroidClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Connection was lost!",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                System.out.println("Connection was lost!");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                System.out.println("Message Arrived!: " + topic + ": " + message.getPayload());
                mqttMsgReceived(message.getPayload());
            }


            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                System.out.println("Delivery Complete!");
            }
        });

        final MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(false);

        try {
            mqttAndroidClient.connect(mqttConnectOptions, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    System.out.println("Connection Success!");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, "Connected to server!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                    try {
                        System.out.println("Subscribing to" + subscriptionTopic);
                        mqttAndroidClient.subscribe(subscriptionTopic, 0);
                    } catch (MqttException ex) {

                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    System.out.println("Connection Failure!");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context,"Failed for connect to server!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        } catch (MqttException ex) {

        }
    }

    static public void disConnect(){
        if(mqttAndroidClient != null) {
            try {
                mqttAndroidClient.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    static private void mqttMsgReceived(byte[] payload){
        if(payload[0] == -1 & payload[1] == -1) {
            PacketHandler.receivedNodeData(payload);
        }
    }

    public String getServerUri() {
        return serverUri;
    }

    public void setServerUri(String serverUri) {
        this.serverUri = serverUri;
    }

    static public void publishMessage(byte [] payload){
        if(mqttAndroidClient != null)
        try {
            MqttMessage message = new MqttMessage();
            message.setPayload(payload);
            mqttAndroidClient.publish(publishTopic, message);
        } catch (MqttException e) {
            System.err.println("Error Publishing: " + e.getMessage());
            e.printStackTrace();
        }
    }

}
