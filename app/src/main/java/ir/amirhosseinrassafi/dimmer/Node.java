package ir.amirhosseinrassafi.dimmer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

public class Node extends AppCompatActivity {

    SeekBar dimmer1, dimmer2;
    String node_num ;
    NodeData nd;
    Integer [] dimmer ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node);
        node_num = getIntent().getStringExtra("Node_Number");

        nd = NodeLib.getNodeData(Integer.valueOf(node_num));

        setTitle("Node "+nd.getNodeID().toString());

//        System.out.println("node number:" + node_num);
//        System.out.println("Node : " + nd);
        dimmer = new Integer[2];
        dimmer[0] = new Integer("5");
        dimmer[1] = new Integer("5");

        dimmer1 = (SeekBar) findViewById(R.id.dimmer1);
        dimmer2 = (SeekBar) findViewById(R.id.dimmer2);


        Byte temp[] = nd.getDimmers();
        dimmer1.setProgress(temp[0]);
        dimmer2.setProgress(temp[1]);

        dimmer1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                System.out.print(seekBar.getProgress());
                nd.setDimmers(new Byte[] {((byte) seekBar.getProgress()), nd.getDimmers()[1]});
                PacketHandler.sendNodeData(nd);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        dimmer2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                System.out.print(seekBar.getProgress());
                nd.setDimmers(new Byte[] {nd.getDimmers()[0], ((byte) seekBar.getProgress())});
                PacketHandler.sendNodeData(nd);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        dimmer1.setOnClickListener(new LedClick(2, Boolean.valueOf(temp[1].toString())));
        UiHandler handler = new UiHandler() {
            @Override
            public void handle() {
                upDateNode();
            }
        };
        PacketHandler.setHandler(handler);
        upDateNode();
    }

    public void upDateNode()
    {
//        Byte temp[] = nd.getLeds();
//        if(temp[0] == 1) {
//            led1.setImageResource(R.drawable.ledon);
//        }
//        else
//        {
//            led1.setImageResource(R.drawable.ledoff);
//        }
//
//        if(temp[1] == 1) {
//            led2.setImageResource(R.drawable.ledon);
//        }
//        else
//        {
//            led2.setImageResource(R.drawable.ledoff);
//        }
    }

    private class LedClick implements View.OnClickListener {
        private int id;
        private boolean state;

        public LedClick(int ledId, boolean ledState)
        {
            id = ledId;
            state = ledState;
        }
        @Override
        public void onClick(View v) {

            if(state) {
                //((ImageButton) v).setImageResource(R.drawable.ledoff);
                state = false;
            }
            else
            {
                //((ImageButton) v).setImageResource(R.drawable.ledon);
                state = true;
            }
            if(state == true)
                dimmer[id-1] = 1;
            else
                dimmer[id-1] = 0;

            Byte[] temp = new Byte[2];
            temp[0] = dimmer[0].byteValue();
            temp[1] = dimmer[1].byteValue();
            nd.setDimmers(temp);
            PacketHandler.sendNodeData(nd);
            //upDateNode();
        }
    }
}