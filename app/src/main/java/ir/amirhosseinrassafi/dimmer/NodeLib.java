package ir.amirhosseinrassafi.dimmer;

import java.util.ArrayList;

public class NodeLib {

    static private ArrayList<NodeData> nodes = new ArrayList<>() ;

    private NodeLib()
    {
    }

    static public void addNode(NodeData nd)
    {
        nodes.add(nd);
    }

    static public Boolean removeNode(NodeData nd)
    {
        return nodes.remove(nd);
    }

    static public Boolean removeNode(int nodeID)
    {
        for (NodeData temp : nodes)
        {
            if(temp.getNodeID() == nodeID)
                return nodes.remove(temp);
        }
        return false;
    }

    static public NodeData getNodeData(int nodeID)
    {
        for (NodeData temp:nodes)
        {
            if(temp.getNodeID() == nodeID)
                return temp;
        }
        return null;
    }
}
